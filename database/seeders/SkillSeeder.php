<?php

namespace Database\Seeders;

use App\Models\Skill;
use Illuminate\Database\Seeder;

class SkillSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Skill::create([
            'name' => 'Data Analysis'
        ]);
        Skill::create([
            'name' => 'Business Intelligence'
        ]);
        Skill::create([
            'name' => 'Machine Learning'
        ]);
        Skill::create([
            'name' => 'Data Visualizations'
        ]);
        Skill::create([
            'name' => 'Deep Learning'
        ]);
        Skill::create([
            'name' => 'Statistics'
        ]);
        Skill::create([
            'name' => 'Power Bi'
        ]);
        Skill::create([
            'name' => 'NumPy'
        ]);
        Skill::create([
            'name' => 'Keras'
        ]);
        Skill::create([
            'name' => 'TensorFlow'
        ]);
        Skill::create([
            'name' => 'Pandas'
        ]);
        Skill::create([
            'name' => 'OpenCV'
        ]);
        Skill::create([
            'name' => 'Writing and Executing Test Cases and Scenarios'
        ]);
        Skill::create([
            'name' => 'Test Design'
        ]);
        Skill::create([
            'name' => 'Test Design'
        ]);
        Skill::create([
            'name' => 'Translating Manual Test Cases Into Automation Scripts'
        ]);
        Skill::create([
            'name' => 'Quality Assurance'
        ]);
        Skill::create([
            'name' => 'Waterfall and SCRUM Methodology'
        ]);
        Skill::create([
            'name' => 'C#'
        ]);
        Skill::create([
            'name' => 'Kiwi'
        ]);
        Skill::create([
            'name' => 'Selenium Web Driver'
        ]);
        Skill::create([
            'name' => 'Illustrator'
        ]);
        Skill::create([
            'name' => 'Photoshop'
        ]);
        Skill::create([
            'name' => 'InDesign'
        ]);
        Skill::create([
            'name' => 'XD'
        ]);
        Skill::create([
            'name' => 'LightRoom'
        ]);
        Skill::create([
            'name' => 'Typography'
        ]);
        Skill::create([
            'name' => 'Branding'
        ]);
        Skill::create([
            'name' => 'Poster Design'
        ]);
        Skill::create([
            'name' => 'Logo Design'
        ]);
        Skill::create([
            'name' => 'Package Design'
        ]);
        Skill::create([
            'name' => 'Digital Marketing Strategy'
        ]);
        Skill::create([
            'name' => 'Social Media Marketing'
        ]);
        Skill::create([
            'name' => 'Facebook and Instagram Ads'
        ]);
        Skill::create([
            'name' => 'Google Ads'
        ]);
        Skill::create([
            'name' => 'Copywriting'
        ]);
        Skill::create([
            'name' => 'Content Marketing'
        ]);
        Skill::create([
            'name' => 'Landing Pages'
        ]);
        Skill::create([
            'name' => 'Lead Generation'
        ]);
        Skill::create([
            'name' => 'E-mail Marketing'
        ]);
        Skill::create([
            'name' => 'Search Engine Optimization'
        ]);
        Skill::create([
            'name' => 'HTML'
        ]);
        Skill::create([
            'name' => 'CSS'
        ]);
        Skill::create([
            'name' => 'Bootstrap'
        ]);
        Skill::create([
            'name' => 'JavaScript'
        ]);
        Skill::create([
            'name' => 'jQuery'
        ]);
        Skill::create([
            'name' => 'AJAX'
        ]);
        Skill::create([
            'name' => 'PHP'
        ]);
        Skill::create([
            'name' => 'ReactJS'
        ]);
        Skill::create([
            'name' => 'GIT'
        ]);
        Skill::create([
            'name' => 'UX/UI'
        ]);
        Skill::create([
            'name' => 'MySQL'
        ]);
        Skill::create([
            'name' => 'InVision Studio'
        ]);
        Skill::create([
            'name' => 'Figma'
        ]);
        Skill::create([
            'name' => 'SQL'
        ]);
        Skill::create([
            'name' => 'Data Warehouse'
        ]);
        Skill::create([
            'name' => 'AWS Management Control'
        ]);
        Skill::create([
            'name' => 'Big Data'
        ]);
        Skill::create([
            'name' => 'Database Management'
        ]);
        Skill::create([
            'name' => 'Postman'
        ]);
        Skill::create([
            'name' => 'Apache'
        ]);
        Skill::create([
            'name' => 'JMeter'
        ]);
        Skill::create([
            'name' => 'Google Analytics'
        ]);
    }
}
